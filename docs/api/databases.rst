Databases
=========

.. currentmodule:: pglift.databases

Module :mod:`pglift.databases` exposes the following API to manipulate
PostgreSQL databases:

.. autofunction:: create
.. autofunction:: alter
.. autofunction:: apply
.. autofunction:: exists
.. autofunction:: list
.. autofunction:: describe
.. autofunction:: drop
.. autofunction:: run
