.. currentmodule:: pglift.conf

PostgreSQL configuration management
===================================

.. autofunction:: make
.. autofunction:: info
.. autofunction:: read
