.. currentmodule:: pglift.cmd

Command execution
=================

.. autofunction:: run
.. autofunction:: start_program
.. autofunction:: terminate_program
.. autofunction:: status_program
